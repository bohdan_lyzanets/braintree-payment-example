(function () {
  'use strict';

  angular.module('app').controller('PaymentController', ['$q', 'paymentService', PaymentController]);

  function PaymentController($q, paymentService) {
    this.$q = $q;
    this.paymentService = paymentService;

    this.init();
  }

  PaymentController.prototype.init = function initPayment() {
    var self = this;

    this.paymentService.getClient()
        .then(function (paymentClient) {
          self.paymentClient = paymentClient;
        })
        .catch(function () {
          // TODO: handle error
        });

    this.creditCard = {};
  };


  PaymentController.prototype.submit = function submit(form) {
    var self = this;

    if (form.$invalid) {
      return;
    }

    this._submitWithPayment(this.submitData, this.creditCard)
        .then(function() {
          // TODO: on success
        })
        .catch(function() {
          // TODO: on fail
        });
  };

  PaymentController.prototype._submitWithPayment = function _submitWithPayment(submitData, creditCard) {
    var self = this;

    return this.paymentService
        .tokenizeCard(this.paymentClient, creditCard)
        .then(function (paymentNonce) {
          return self._submit(submitData, paymentNonce);
        })
        .catch(function (response) {
          // TODO: handle tokenize card error
          return self.$q.reject(response);
        });
  };

  PaymentController.prototype._submit = function _submit(submitData, paymentMethodNonce) {
    var self = this;

    // NOTE: send your submit data with paymentMethodNonce to server side.
    // NOTE: Handle data and paymentMethodNonce by braintree backend API
    return this.paymentService.submit(submitData, paymentMethodNonce);
  };

})();
