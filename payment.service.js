(function () {
  'use strict';

  angular.module('app').service('PaymentService', ['$q', '$http', 'ENV', PaymentService]);

  function PaymentService($q, $http, ENV) {
    this.$q = $q;
    this.$http = $http;
    // brintree from global scope
    this.braintree = braintree;

    this.ENV = ENV;
  }

  PaymentService.prototype.getClientToken = function getClientToken() {
    return this.$http.get(this.ENV.apiEndPoint + 'payment/braintree/clienttoken');
  };

  PaymentService.prototype.getClient = function getClient() {
    var braintree = this.braintree;

    return this.getClientToken().then(function (token) {
      return new braintree.api.Client({ clientToken: token });
    });
  };

  PaymentService.prototype.tokenizeCard = function tokenizeCard(paymentClient, creditCard) {
    return this.$q(function(resolve, reject) {
      paymentClient.tokenizeCard(creditCard, function (err, nonce) {
        return err ? reject(err) : resolve(nonce);
      });
    });
  };


  PaymentService.prototype.submit = function submit(submitData, paymentMethodNonce) {
    return this.$http.post(this.ENV.apiEndPoint + 'payment/submit/path', {
      data: submitData,
      paymentMethodNonce: paymentMethodNonce
    })
  };

})();